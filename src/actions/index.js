import { Actions } from 'react-native-router-flux';

import {
    SAMPLE_CONST,
    LOGIN_USER,
    LOGIN_USER_SUCCESS
} from './types';

export const sampleAction = (param) => {
    return {
        type: SAMPLE_CONST,
        payload: param
    }
}

export const loginUser = () => {
    return (dispatch) => {
        loginUserSuccess(dispatch, null);
    }
}

const loginUserSuccess = (dispatch, user) => {
    dispatch({
        type: LOGIN_USER_SUCCESS,
        payload: user
    });

    Actions.main({ type: 'reset' });
}