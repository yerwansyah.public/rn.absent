import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Platform } from 'react-native';
import { Card, CardItem, Content, View, Text, Input, InputGroup, Icon, Container, Button } from 'native-base';


class LoginInput extends Component {
    onPress() {
        this.props.loginUser();
    }

    render() {
        return (
            <Container>
                <Content>
                    <InputGroup>
                        <Icon name="ios-person" />
                        <Input placeholder="USERNAME" />
                    </InputGroup>
                    <InputGroup>
                        <Icon name="ios-unlock" />
                        <Input placeholder="PASSWORD" />
                    </InputGroup>
                </Content>
            </Container>
        );
    }
}

export default connect()(LoginInput);