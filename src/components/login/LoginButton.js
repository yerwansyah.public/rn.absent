import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Platform } from 'react-native';
import { Card, CardItem, Content, View, Text, Input, InputGroup, Icon, Container, Button } from 'native-base';
import { loginUser } from '../../actions';

class LoginButton extends Component {
    onPress() {
        this.props.onSubmitForm();
    }

    render() {
        return (
            <Container >
                <Content>
                    <Button
                        block bordered rounded info
                        onPress={this.onPress.bind(this)}
                    >Login</Button>
                </Content>
            </Container>
        );
    }
}

export default connect()(LoginButton);