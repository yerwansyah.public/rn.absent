import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Platform } from 'react-native';
import { Card, CardItem, Content, View, Text, Input, InputGroup, Icon, Container, Button } from 'native-base';
import LoginInput from './LoginInput';
import LoginButton from './LoginButton';
import { loginUser } from '../../actions';

class LoginForm extends Component {
    onPress() {
        this.props.loginUser();
    }

    render() {
        return (
            <Container>
                <Content padder>
                    <Card>
                        <CardItem>
                            <LoginInput />
                        </CardItem>
                        <CardItem>
                            <LoginButton onSubmitForm={this.onPress.bind(this)} />
                        </CardItem>
                    </Card>
                </Content>
            </Container>

        );
    }
}

{/*<Card >
    <CardItem>
        <InputGroup>
            <Icon name="ios-person" />
            <Input placeholder="USERNAME" />
        </InputGroup>
        <InputGroup>
            <Icon name="ios-unlock" />
            <Input placeholder="PASSWORD" />
        </InputGroup>
    </CardItem>

    <CardItem >
        <Button
            block bordered rounded info
            onPress={this.onPress.bind(this)}
        >Success</Button>
    </CardItem>
</Card>*/}

export default connect(null, { loginUser })(LoginForm);