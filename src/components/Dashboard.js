import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Platform } from 'react-native';
import { Card, CardItem, Footer, Content, Title, FooterTab, Tabs, View, Header, Icon, Text, Input, InputGroup, Container, Button } from 'native-base';
import AbsentDetails from './absent/AbsentDetails';
import AbsentLocation from './absent/AbsentLocation';
import AbsentButton from './absent/AbsentButton';
import { } from 'react-native-vector-icons/FontAwesome';

class Dashboard extends Component {
    render() {
        return (
            <Container>
                <Content padder>
                    <Card>
                        <CardItem>
                            <AbsentDetails />
                        </CardItem>
                        <CardItem>
                            <AbsentLocation />
                        </CardItem>
                        <CardItem >
                            <AbsentButton />
                        </CardItem>
                    </Card>
                </Content>
                <Footer>
                    <FooterTab theme={{ iconFamily: 'FontAwesome' }}>
                        <Button active={true} >
                            Absent
                            <Icon name="sign-in" />
                        </Button>
                        <Button active={false} >
                            History
                            <Icon name="list" />
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

export default Dashboard;