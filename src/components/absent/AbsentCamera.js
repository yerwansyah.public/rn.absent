import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Platform, Dimensions } from 'react-native';
import { Card, CardItem, Footer, Content, Title, FooterTab, Tabs, View, Header, Icon, Text, Input, InputGroup, Container, Button } from 'native-base';
import Camera from 'react-native-camera';

class AbsentCamera extends Component {

    takePicture() {
        this.camera.capture()
            .then((data) => console.log(data))
            .catch(err => console.error(err));
    }

    render() {
        return (
            <Container>
                <Content>
                    <Camera
                        ref={(cam) => {
                            this.camera = cam;
                        }}
                        style={styles.preview}
                        aspect={Camera.constants.Aspect.fill}>
                        <Text style={styles.capture} onPress={this.takePicture.bind(this)}>[CAPTURE]</Text>
                    </Camera>
                </Content>
            </Container>
        );
    }
}

const styles = {
    container: {
        flex: 1
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        color: '#000',
        padding: 10,
        margin: 40
    }
};


export default AbsentCamera;