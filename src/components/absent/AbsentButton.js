import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Platform } from 'react-native';
import { Card, CardItem, Footer, Content, Title, FooterTab, Tabs, View, Header, Icon, Text, Input, InputGroup, Container, Button } from 'native-base';

class AbsentButton extends Component {
    render() {
        return (
            <Container>
                <Content>
                    <Button block bordered rounded info>Absent In</Button>
                </Content>
            </Container>
        );
    }
}

export default AbsentButton;