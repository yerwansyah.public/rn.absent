import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Platform } from 'react-native';
import { Card, CardItem, Footer, Content, Title, FooterTab, Tabs, View, Header, Icon, Text, Input, InputGroup, Container, Button } from 'native-base';

class AbsentLocation extends Component {
    state = {
        initialPosition: [],
        lastPosition: [],
    };

    watchID = null;

    componentDidMount() {
        console.log('componentDidMount');
        this.watchID = navigator.geolocation.watchPosition((position) => {
            console.log('watch');
            var lastPosition = position.coords;
            this.setState({ lastPosition });
        },
            (error) => alert(JSON.stringify(error))
        );

        console.log('watchID', this.watchID);
    }

    componentWillUnmount() {
        console.log('componentWillUnmount');
        navigator.geolocation.clearWatch(this.watchID);
    }

    render() {
        console.log('render');
        console.log('initialPosition', this.state.initialPosition);
        console.log('lastPosition', this.state.lastPosition);
        return (
            <Container>
                <Content>
                    <Text>{this.state.initialPosition.latitude} / {this.state.initialPosition.longitude}</Text>
                    <Text>{this.state.lastPosition.latitude} / {this.state.lastPosition.longitude}</Text>
                    <Text>watchID {this.watchID}</Text>
                </Content>
            </Container>
        );
    }
}

export default AbsentLocation;