import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import { Text, View, Route } from 'react-native';
import LoginForm from './components/login/LoginForm';
import Dashboard from './components/Dashboard';
import AbsentCamera from './components/absent/AbsentCamera';

const RouterComponent = () => {
    return (
        <Router sceneStyle={{ paddingTop: 65 }}>
            <Scene key="auth">
                <Scene
                    key="login"
                    component={LoginForm}
                    title="Absent Login"
                />
            </Scene>
            <Scene key="main" hideNavBar hideTabBar>
                <Route key="camera" component={AbsentCamera} showNavigationBar={false}>

                </Route>
                <Scene hideNavBar hideTabBar
                    key="dashboard"
                    component={AbsentCamera}
                    title="Dashboard"
                />
                
            </Scene>

        </Router>
    );
};

export default RouterComponent;